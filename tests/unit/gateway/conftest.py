import pytest
import responses

from piperci_noop_gateway.function.faas_app import app as papp
from piperci_noop_gateway.function.config import Config
from typing import Tuple


@pytest.fixture
def app():  # required by pytest_flask
    return papp


@pytest.fixture
def config():
    return Config


@pytest.fixture
def task_post_executor_url_response(config) -> Tuple[Tuple, dict]:
    return (
        (responses.POST, config["executor_url"].format(endpoint=config["endpoint"])),
        dict(),
    )
