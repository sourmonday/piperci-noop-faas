import pytest

from piperci_noop_executor.function.faas_app import app as papp
from piperci_noop_executor.function.config import Config


@pytest.fixture
def app():  # required by pytest_flask
    return papp


@pytest.fixture
def config():
    return Config
