from piperci.faas.exceptions import PiperDelegateError, PiperError


def handle(request, task, config):
    """
    Entrypoint to the noop FaaS.
    :param request: The request object from Flask
    :param task: The ThisTask instance for this class required for this faas
    :return: (response_body, code)
    """

    try:
        task.info('Noop faas gateway handler.handle called successfully')
    except PiperError as e:
        return {'error': f'{str(e)}: no delegate was attempted'}, 400

    try:
        task.delegate(config['executor_url'], task.task)
        return task.complete('Noop gateway completed successfully')
    except PiperDelegateError as e:
        return {'error': f'{str(e)}'}, 400
