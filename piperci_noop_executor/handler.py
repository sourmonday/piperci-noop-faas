from flask import Request
from pathlib import Path
from typing import Dict, Tuple, Union

from piperci.faas.exceptions import PiperError
from piperci.faas.this_task import ThisTask


def handle(
    request: Request, task: ThisTask, config: Dict
) -> Tuple[Union[Dict, str], int]:
    """
    Entrypoint to the noop faas async executor function.
    :param request: The request object from Flask
    :param task: The ThisTask instance for this class required for this faas
    :param config:
    :return: (response_body, code)
    """
    task.info(str(task.task))
    # See piperci.faas.this_task for gman.task interface usage docs
    try:
        task.info("Noop faas gateway handler.handle called successfully")

        # ## Replace below path with desired upload path
        artfile_path = f'test_artifact_{task.task["task"]["task_id"]}'
        execute(request, task, Path(artfile_path))

        task.artifact(artfile_path)
        task.complete("Noop faas complete")
        return {"test": "test"}, 200
    except PiperError:
        return "", 400


def execute(request: Request, task: ThisTask, write_to: Union[Path, str]):
    # ## Replace below with task execution logic
    artifact = f'noop artifact content {task.task["task"]["task_id"]}'

    with open(write_to, "w") as artfile:
        artfile.write(artifact)
